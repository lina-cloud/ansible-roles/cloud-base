Cloud Base
=========

Install base packages and creates a new user and install oh my zsh for the new user and the root user

Requirements
------------

see Role Variables

Role Variables
--------------

`username` is required
`ssh_key` is required for more information see Ansible Docs ansible.posix.authorized_key

Dependencies
------------

None

Example Playbook
----------------

This role is only available via this git repo 

    - hosts: servers
      roles:
         - role: cloud-base
           vars:
            username: exsample
            ssh_key: https://gitlab.com/username.key

License
-------

Mozilla Public License 2.0

